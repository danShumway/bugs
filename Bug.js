/*
	Name: Bug
	Author: Dan Shumway and Tony Hoang
	Last Modified: 10/17/2013
	Description: this.constructor that creates a Bug
*/


//Bugs are the things on the screen.
function Bug(xpos, ypos, rad, sped) {

			//you can actually make bugs without specifying any variables.  
			//I don't think it's used anywhere in our current code, but it was pretty essential at some point in development.
			if(!xpos)
			{
				this.x = Math.random()*canvas.width; //Position
				this.y = Math.random()*canvas.height;
				this.radius = 4; //Size (also maturity)
				this.speed = 30; //Speed is speed.
			}
			else
			{
				this.x = xpos;
				this.y = ypos;
				this.radius = rad;
				this.speed = sped;
			}

			//The other variables.
			this.alive = true; //Bugs can be corpses.
			this.direction = Math.random()*360; //Where are you facing?
			this.lastDirectionChange = 0; //How long have you been facing there.
			this.removeMe = false; //Do you want to not exist anymore (different than dying)
			this.growthMultiplier = Math.random(); //Bugs grow at different rates.
			this.lifeSpan = Math.random() * 50; //Bugs have predestined lifespans, but those lifespans can be cut short.
			this.pregnant = 0; //Are you pregnant?  How many babies do you have inside you?
			this.pregnantCount = 0; //How long until give birth to one of the (more than zero) babies in pregnantCount?
			this.poisoned = 0; //Are you poisoned?  How posioned on a scale of 0 to infinity?
			this.colony = 0; //Not used.  In the theoretical future, ants could belong to a colony.
			this.quad; //What node of the quadtree are you in?
			this.decomposeTimer = 0; //how long has the corpse been decomposing
			this.lastCollision;

			//Accessors for variables. 


			//Setters.
			
			//make this ant toxic
			this.poisonMe = function() { this.poisoned += 1; }
			
			//kill off this ant
			this.kill = function(){ this.alive = false; }



			//update code.
			this.update = function() {

				//Living code.
				if(this.alive) {
					//Movement
					//x += (Math.random() * 30 - 15) * dt;
					//y += (Math.random() * 30 - 15) * dt;

					//Choose a direction
					this.lastDirectionChange = this.lastDirectionChange - 20 * dt;
					if( this.lastDirectionChange <= 0 ) 
					{
						this.direction = this.direction + (Math.random()*44 - 22);
						this.lastDirectionChange = Math.random()* 20 + 10;
					}

					this.x += this.speed * dt * Math.cos(this.direction);
					this.y += this.speed * dt * Math.sin(this.direction);
					
					//wrap around edges
					this.checkBoundaries();
					
					//Grow as you age.
					if(this.lifeSpan > 0)
					{
						this.radius += .15 * dt * this.growthMultiplier;
						this.lifeSpan -= 1 * dt * this.growthMultiplier;
						this.speed += 1 * dt * this.growthMultiplier;
					}
					else
					{
						this.alive = false;
					}

					//If you're poisoned, you gunna have trouble.
					this.lifeSpan -= 2 * this.poisoned * dt;
					this.speed -= 1 * this.poisoned * dt;

					//If you're pregnant, maybe you'll give birth
					if(this.pregnant > 0)
					{
						if(Math.random()*700 > 690) this.giveBirth();
					}

				} else {
					//Dead ants just sort of sit around for a bit
					//Will decompose over time and rot themselves off the canvas
					
						
					
					
				}


			}
			
			//The bugs will wrap themselves within the dimensions of the canvas
			this.checkBoundaries = function()
			{
				if (this.x < 0)
					{
						this.x = canvas.width;
					}
					
				if (this.x > canvas.width)
				{
					this.x = 0;
				}
				
				if (this.y < 0)
				{
					this.y = canvas.height;
				}
				
				if (this.y > canvas.height)
				{
					this.y = 0;
				}
			}
			//draw code.
			this.draw = function() {
				ctx.beginPath();

				//If you're supposed to be showing a color.
				if(window.showColor == true)
				{
					//If you're alive.
					if(this.alive) 
					{
						//And a child
						if(this.radius < 4) 
							//And poisoned
							if(this.poisoned > 0) 
								ctx.fillStyle = "orange";
								
							//Or a living child that's not poisoned.
							else
								ctx.fillStyle = "red";
						else
						{
							//Or alive adult pregnant
							if(this.pregnant > 0) 
								//poisoned
								if (this.poisoned > 0)
									ctx.fillStyle = "purple";
									
								//not poisoned
								else 
									ctx.fillStyle = "blue";
									
							//alive adult not pregnant poisoned
							else 
								if(this.poisoned > 0)
									ctx.fillStyle = "green";
									
								//alive adult not pregnant not poisoned
								else 
								ctx.fillStyle = "black";
						}
					}
					else 
					{	//Dead and poisoned
						if(this.poisoned > 0)
							ctx.fillStyle = "silver"
						//Dead and just dead.
						else 
							ctx.fillStyle = "gray";
					}
				}
				else //If you're not supposed to be showing a color, just draw black.
				{
					ctx.fillStyle = "black";
				}
				ctx.arc(this.x, this.y, this.radius, 0, 2*Math.PI, true);
				ctx.fill();
			}


			//Check to see if two bugs are colliding.  Uses squares.
			this.checkCollision = function(object1, object2) {

				var xdistance = Math.abs(object1.x - object2.x);
				var ydistance = Math.abs(object1.y - object2.y);
				//get the two origins.  If one of them is within distance of the other one.
				if(
					//x collision.
					xdistance < (object2.radius + object1.radius) &&
					//y collision.
					ydistance < (object2.radius + object1.radius)
				){

					//Collision happened.
					//console.log(object1 === object2);
					 //console.log((object1.x - object2.x) + " " + (object1.radius - object2.radius));
					return true;
				}
				//Collision didn't happen.
				return false;
			}

			//Does a collision.
			this.doCollision = function(object)
			{
				this.lastCollision = object;
				if(this.alive)
				{
					//If it's a dead body
					if(object.alive == false)
					{

						//If you're big enough to eat it.
						if(this.radius - object.radius >= -1) {
							object.getEaten(this);
							//If you're still growing
							if(this.radius < 4)
							{
								this.radius += object.radius / 5;
								this.speed += object.radius / 7;
							}
						}
						else
						{
							//Feed off it.
							object.radius = object.radius - (this.radius/2 * dt);
							//If you're still growing
							if(this.radius < 4)
							{
								this.radius += object.radius / 10 * dt;
							}
						}

					}
					//Eat each other too as a child.  It's the best way to grow.
					//Once you become an adult (radius 4), you won't get to eat your siblings, unless they're dead.
					else if(object.radius < this.radius && this.radius <= 4)
					{
						object.getEaten(this);
						this.radius += object.radius / 5;
						this.speed += object.radius / 7;
					}
					//If you run into a parent, try and kill it if you're able.
					else if(object.radius > this.radius + .5 && this.radius > 1 && this.radius < 4)
					{
						//Attempt to poison it.
						if(Math.random()*700 > 698) object.poisonMe();
					}

					//Attempt to reproduce if you've hit something that's alive.  
					//Bugs need to be a certain age before they can reproduce.
					//A bug that's already pregnant can't get pregant again until they've had all their births.
					if(object.alive && this.radius >= 4 && object.radius >= 4 && this.pregnant == 0)
					{
						//The parent gets pregnant.
						this.pregnant = this.radius;
						this.radius += 1;
						this.speed -= 2;

						//If an ant has gotten pregnant too many times in a row, it will probably produce diseased children (population control)
						this.pregnantCount ++ ;
						//Not implemented, but that was the intention anyway.
					}

				}
			}


			//Have a baby bug.
			this.giveBirth = function()
			{
				var distance;
				for(var i = 0; i < 2; i++)
				{
					distance = Math.random()*(this.radius + 1)*4 - (this.radius + 1)*2;
					bugs.push(new Bug(this.x + distance, this.y + distance, this.radius/4, 2));
					quadTree.top.pushIn(bugs[bugs.length - 1]);
					//poisoned bugs give birth to stillborns.
					if(this.poisoned > 0)
						bugs[bugs.length - 1].kill();
						//If the parent is really poisoned, it's babies become toxic.
						if(this.poisoned > 2)
							bugs[bugs.length - 1].poisonMe();
				}
				this.pregnant --;
			}

			//Get eaten by another bug.
			this.getEaten = function(object)
			{
				//If you're poisoned.
				if(this.poisoned > 1)
					//The eater has a chance to be poisoned also.
					if(Math.random() * this.poisoned > .3) object.poisonMe();
				this.removeMe = true;
			}
		}