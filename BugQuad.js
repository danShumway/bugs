/*
	Name: Bugquad
	Author: Dan Shumway and Tony Hoang
	Last Modified: 10/17/2013
	Description: Function constructor that sets up the quadTree as an extra feature
*/

"use strict"

function Bugquad()
{

	//The top section (the size of the entire screen.)
	this.top = new Node(0, 0, 1000, 600, 0);


	//Begin Node constructor.
	function Node(x, y, width, height, depth)
	{
		//Normal stuff.  A node is essentially a square space.
		this.x = x;
		this.y = y;
		this.Width = width;
		this.Height = height;
		this.Depth = depth;

		this.children = [];

		//Some of the more hacky bits.  It's terrible practice, but the quad tree's children never go away.
		//you'll always have a full quad tree, even if there's nothing inside of it.
		if(this.Depth < 5)
		{
			for(var i = 0; i < 4; i++)
			{
				//Make the quads.
				this.children.push( new Node(this.x + (i%2)*this.Width/2, this.y + Math.floor(i/2) * this.Height/2, this.Width/2, this.Height/2, this.Depth + 1));

			}
		}
		else
		{
			//If you're at max depth, stop making children.
			for(var i = 0; i < 4; i++)
			{
				this.children.push(undefined);
			}
		}

		this.bugs = [];


		//draw function - used for visualization.
		this.draw = function()
		{
			ctx.strokeStyle = 'rgb(' + this.Depth*40 +',100,100)';
			if(this.bugs.length != 0)
			{
				ctx.strokeRect(this.x, this.y, this.Width, this.Height);
			}
			//debugger;

			for(var i = 0; i < 4; i++)
			{
				if(this.children[i] != undefined)
				{
					this.children[i].draw();
				}
			}
		} //End draw


		//Resets the tree.  Another hacky solution.
		this.clear = function()
		{
			this.bugs = [];
			for(var i = 0; i < 4; i++)
			{
				if(this.children[i] != undefined)
				{
					this.children[i].clear();
				}
			}
		}

		this.deleteBug = function(bug)
		{
			//A bug should only be added to a quad once.
			//I'm going to assume that is true, and break as soon as I find the correct bug.
			for(var i = 0; i < this.children.length; i++)
			{
				if( this.children[i] === bug)
				{
					this.children.splice(i, 1);
					return true; //Successfully removed.
				}
			}
			//element did not exist.
			return false;
		}

		//Push a bug into a quad.
		this.pushIn = function(bug)
		{
			//Keep it in the above.
			this.bugs.push(bug);
			if(this.isContained(bug))
			{
				bug.quad = this;
			}
			//Check to see if it could be put into a child.
			for(var i = 0; i < 4; i++) //Try and add it to the children first.
			{
				if(this.children[i] != undefined)
				{
					if(this.children[i].overlaps(bug))
					{
						this.children[i].pushIn(bug);
					}
				}
			}
		}


		//Push a bug into a quad.
		this.pushOld = function(bug)
		{
			//Check to see if it could be put into a child.
			var added = false;
			for(var i = 0; i < 4; i++) //Try and add it to the children first.
			{
				if(this.children[i] != undefined)
				{
					if(this.children[i].isContained(bug))
					{
						this.children[i].push(bug);
						added = true;
					}
					else if(this.children[i].overlaps(bug))
					{
						//You still might collide with those areas, so you still need to be added.
						this.children[i].push(bug);
					}
				}
			}

			//Didn't work?  Well, it still belongs to you.
			if(!added)
			{
				this.bugs.push(bug);
				bug.setquad(this);
				//console.log(this.bugs.length);
			}
		}

		//Check to see if a bug is in a quad.
		this.isContained = function(bug)
		{
			//If it's within the boundaries of the quad, return true.
			if(bug.x > this.x
				&& bug.y > this.y
				&& bug.x + bug.radius < this.x + this.Width
				&& bug.y + bug.radius < this.y + this.Height)
			{
				return true;
			}
			return false;
		}

		//A bug might not be contained in its children, but does it overlap with its children?
		this.overlaps = function(bug)
		{
			var xdistance = Math.abs(bug.x - (this.x + this.Width/2));
			var ydistance = Math.abs(bug.y - (this.y + this.Height/2));
			//get the two origins.  If one of them is within distance of the other one.
			if(
				//x collision.
				xdistance < this.Width/2 + bug.radius/*Math.max(this.width, bug.radius)*/ &&
				//y collision.
				ydistance < this.Height/2 + bug.radius/*Math.max(this.height, bug.radius)*/
			){
				//Collision happened
				return true;
			}
			//Collision didn't happen.
			return false;
		}

		this.getCollisionsToCheck = function()
		{
			//Not sure if this is the most efficient way to do this.  I should run some tests on concat.
			var toReturn = this.bugs;
			for(var i = 0; i < 4; i++)
			{
				if(this.children[i] != undefined)
				{
					toReturn = toReturn.concat(this.children[i].getCollisionsToCheck());
					//temp = toReturn;
				}
			}

			return toReturn;
		}

		this.getCollisionsToCheckNumber = function()
		{
			//Not sure if this is the most efficient way to do this.  I should run some tests on concat.
			var toReturn = this.bugs;
			for(var i = 0; i < 4; i++)
			{
				if(this.children[i] != undefined)
				{
					toReturn = toReturn.concat(this.children[i].getCollisionsToCheck());
										//temp = toReturn;
				}
			}

			return toReturn.length;
		}

	}//End Node
}