/*
	Name: GetInput
	Author: Dan Shumway and Tony Hoang
	Last Modified: 10/17/2013
	Description: Contains the event handlers that were incoporated. 
*/

"use strict"


//Adds in all of our event listeners when the page loads.
window.addEventListener("load", function(){

	//Show color.
	document.getElementById("showColor").addEventListener("change", function(){
		window.showColor = document.getElementById("showColor").checked;
	});

	//Show quad.
	document.getElementById("showquad").addEventListener("change", function(){
		window.showquad = document.getElementById("showquad").checked;
	});

	//Register clicks on the canvas. Adds swarms at the mouse's position
	document.getElementById("canvas").addEventListener("click", function(e) {
		var mouse = getMouse(e);

		var action = document.getElementById("mouseAction");
		action = action[action.selectedIndex].value;

		if(action == "addSwarm")
		{
			for(var i = 0; i < 15; i++)
			{
				bugs.push(new Bug(mouse.x + Math.random()*50 - 25, mouse.y + Math.random()*50 - 25, 1, 2));
			}
		} 
			else if(action == "trackCollisions")
		{
			for(var i = 0; i < window.bugs.length; i++)
			{
				if(Math.abs(mouse.x - window.bugs[i].x) < window.bugs[i].radius + 1 && Math.abs(mouse.y - window.bugs[i].y) < window.bugs[i].radius + 1)
				{
					if(window.bugToTrack != bugs[i]) {
						window.bugToTrack = bugs[i];
						return;//stop there.
					}
					else {
						window.bugToTrack = undefined;
						return;//stop there.
					}
				}
			}
			window.bugToTrack = undefined;//If you don't click on anything cancel it out.
		}

	});




	//translates a mouse position to its location on the canvas.
	function getMouse(e){
		var mouse = {}
		mouse.x = e.pageX - canvas.offsetLeft;
		mouse.y = e.pageY - canvas.offsetTop;
		return mouse;
	}

});